module.exports = {
  root: true,
  parserOptions: {
    ecmaVersion: 2021,
  },
  env: {
    node: true,
    es2021: true,
  },
  extends: [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:promise/recommended",
  ],
  plugins: ["import", "node", "promise"],
  rules: {
    "indent": ["error", 2], // specify 2-space indentation
    "quotes": ["error", "double"], // enforce double quotes
    "semi": ["error", "always"] // require semicolons
  },
};
  